from django.test import Client,TestCase #client module is basically used as a dummy web browser to simulate GET and POST requests on a URL
from django.contrib.auth import get_user_model#to reference our active user
from django.urls import reverse
from .models import Post

class BlogTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username = 'testuser',
            email = 'test@email.com',
            password = 'secret',
        )

        self.post = Post.objects.create(
            title = 'Some Title',
            author = self.user,
            body = 'Awesome description',
        )
    
    def test_string_representation(self):
        post = Post(title = 'Some Title')
        self.assertEqual(str(post), post.title)

    #method to confirm correctness of content
    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'Some Title')
        self.assertEqual(f'{self.post.author}', 'testuser')
        self.assertEqual(f'{self.post.body}', 'Awesome description')
    
    #method to confirm our homepage returns a 200 HTTP status code,contains our body text and uses the correct template
    def test_post_list_view(self):
        response = self.client.get(reverse('home.html'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Awesome description')
        self.assertTemplateUsed(response,'home.html')

    #method to check that 'post_detail.html' works perfectly and that an incorrect page returns error 404
    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'Some Title')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('post_new'),{
            'title' : 'Some Title',
            'body' : 'Awesome description',
            'author' : 'self.user',
        })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Some Title')
        self.assertContains(response,'Awesome description')
    
    def test_post_update_view(self):
        response = self.client.post(reverse('post_edit', args = '1'),{
            'title' : 'Updated Title',
            'body' : ' Updated description',
            })
        self.assertEqual(response.status_code, 302)
    
    def test_post_delete_view(self):
        response = self.client.get(reverse('post_delete' , args = '1'))
        self.assertEqual(response.status_code, 200)

